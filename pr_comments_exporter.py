import argparse
import csv
import requests
import re
from requests.exceptions import HTTPError


def parse_severity(comment):
    # 正则表达式匹配严重程度格式
    severity_pattern = re.compile(
        r'严重程度[：:]\s*(致命|严重|一般|提示)|'
        r'(?:\【|\[)严重程度(?:\】|\])\s*[：:]\s*(致命|严重|一般|提示)|'
        r'(?:\【|\[)(致命|严重|一般|提示)(?:\】|\])'
    )
    matches = severity_pattern.finditer(comment)
    for match in matches:
        # 提取所有的匹配分组
        severity_levels = match.groups()
        # 循环检查每个匹配组是否不为None
        for severity_level in filter(None, severity_levels):
            return severity_level
    # 如果没有找到有效的严重程度，默认为“提示”
    return "提示"

def get_pr_comments(org, repo, pr_id, page=1, per_page=100, comment_type='diff_comment', http_proxy=None):
    api_url = f'https://gitee.com/api/v5/repos/{org}/{repo}/pulls/{pr_id}/comments'
    params = {
        'page': page,
        'per_page': per_page,
        'comment_type': comment_type
    }

    proxies = None
    if http_proxy:
        proxies = {'http': http_proxy, 'https': http_proxy}
    
    try:
        response = requests.get(api_url, headers={'Content-Type': 'application/json;charset=UTF-8'}, params=params, proxies=proxies)
        response.raise_for_status()
        return response.json()
    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')  # Python 3.6+
        return None
    except Exception as err:
        print(f'Other error occurred: {err}')  # Python 3.6+
        return None

def write_comments_to_csv(comments, output_file):
    with open(output_file, mode='a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)
        # Write header if the file is empty
        if file.tell() == 0:
            writer.writerow(["PR url", "Comment URL", "Body", "Severity", "Created At", "Author"])
        for comment in comments:
            writer.writerow([
                comment.get('pull_request_url'),
                comment.get('html_url'),
                comment.get('body'),
                parse_severity(comment.get('body')),
                comment.get('created_at'),
                comment.get('user', {}).get('login')
            ])

def process_prs(pr_urls, output_file, http_proxy=None):
    for pr_url in pr_urls:
        # Extract the org, repo, and PR ID from URL using regex
        match = re.match(
            r'https?://gitee\.com/(?P<org>[^/]+)/(?P<repo>[^/]+)/pulls/(?P<pr_id>\d+)', pr_url)
        if not match:
            print(f"Invalid PR URL: {pr_url}")
            continue

        org = match.group('org')
        repo = match.group('repo')
        pr_id = match.group('pr_id')
        
        print(f"Fetching comments for PR {pr_id} in {org}/{repo}...")
        comments = get_pr_comments(org, repo, pr_id, http_proxy=http_proxy)
        if comments:
            write_comments_to_csv(comments, output_file)



def read_prs_from_file(file_path):
    with open(file_path, mode='r', newline='', encoding='utf-8') as file:
        # Here we don't need to check if the line is a digit since we are working with full URLs now
        return [line.strip() for line in file.readlines() if line.strip()]

def main():
    parser = argparse.ArgumentParser(description='Export PR comments from Gitee to a CSV file.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--pr', type=str, help='Pull Request URL')
    group.add_argument('--pr-file', type=str, help='File containing a list of Pull Request URLs')
    parser.add_argument('--output', required=True, help='Output CSV file', type=str)
    parser.add_argument('--proxy', help='HTTP proxy URL', type=str, required=False)


    args = parser.parse_args()

    pr_list = []

    if args.pr:
        pr_list.append(args.pr)
    elif args.pr_file:
        pr_list = read_prs_from_file(args.pr_file)

    if not pr_list:
        print('No valid PR URLs provided.')
        return

    # Make sure to clear previous content if output file exists
    open(args.output, 'w').close()

    process_prs(pr_list, args.output, http_proxy=args.proxy)
    print(f"Comments have been written to {args.output}")

if __name__ == "__main__":
    main()